package si.uni_lj.fri.lrk.lab7;

import androidx.appcompat.app.AppCompatActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import si.uni_lj.fri.lrss.machinelearningtoolkit.MachineLearningManager;
import si.uni_lj.fri.lrss.machinelearningtoolkit.classifier.Classifier;
import si.uni_lj.fri.lrss.machinelearningtoolkit.utils.ClassifierConfig;
import si.uni_lj.fri.lrss.machinelearningtoolkit.utils.Constants;
import si.uni_lj.fri.lrss.machinelearningtoolkit.utils.Feature;
import si.uni_lj.fri.lrss.machinelearningtoolkit.utils.FeatureNominal;
import si.uni_lj.fri.lrss.machinelearningtoolkit.utils.FeatureNumeric;
import si.uni_lj.fri.lrss.machinelearningtoolkit.utils.Instance;
import si.uni_lj.fri.lrss.machinelearningtoolkit.utils.MLException;
import si.uni_lj.fri.lrss.machinelearningtoolkit.utils.Signature;
import si.uni_lj.fri.lrss.machinelearningtoolkit.utils.Value;

public class MainActivity extends AppCompatActivity {

    public static final String TAG = "MainActivity";

    public static final String ACTION_CLASSIFIER_TRAINING = "si.uni_lj.fri.lrk.lab7.TRAIN_CLASSIFIER";

    AccBroadcastReceiver mBcastRecv;

    MachineLearningManager mManager;

    Handler handler;

    Boolean mSensing;
    Boolean mTraining;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mBcastRecv = new AccBroadcastReceiver();

        this.handler = new Handler();

        final Button controlButton = findViewById(R.id.btn_control);

        mSensing = false;
        mTraining = false;

        controlButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mSensing) {
                    controlButton.setText(R.string.txt_start);
                    stopSensing();
                } else {
                    controlButton.setText(R.string.txt_stop);
                    startSensing();
                }
            }
        });

        Switch sw = findViewById(R.id.sw_training);

        sw.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    mTraining = true;

                    findViewById(R.id.tv_select).setVisibility(View.VISIBLE);
                    findViewById(R.id.radioGroup).setVisibility(View.VISIBLE);
                    findViewById(R.id.tv_result).setVisibility(View.INVISIBLE);
                } else {
                    mTraining = false;

                    findViewById(R.id.tv_select).setVisibility(View.INVISIBLE);
                    findViewById(R.id.radioGroup).setVisibility(View.INVISIBLE);
                    findViewById(R.id.tv_result).setVisibility(View.VISIBLE);
                }
            }
        });

        initClassifier();
    }


    @Override
    protected void onStart() {
        super.onStart();

        // Register local broadcast receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mBcastRecv,
                new IntentFilter(AccBroadcastReceiver.ACTION_SENSING_RESULT));
    }


    @Override
    protected void onStop() {
        super.onStop();

        // Unregister local broadcast receiver
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mBcastRecv);
    }

    void startSensing()
    {
        Log.d(TAG,"startSensing()");

        mSensing = true;

        // set Handler to run AccSenseService every five seconds
        handler.post(new Runnable() {
            public void run() {
                Intent intent = new Intent(getApplicationContext(),
                        AccSenseService.class);
                startService(intent);
                handler.postDelayed(this, 5000);
            }
        });

    }

    void stopSensing()
    {

        Log.d(TAG,"stopSensing()");

        mSensing = false;

        this.handler.removeMessages(0);

        View v  = findViewById(R.id.container);
        v.setBackgroundColor(Color.WHITE);
    }


    void initClassifier() {
        Log.d(TAG,"initClassifier");

        // Instantiate the classifier
        try {
            mManager = MachineLearningManager.getMLManager(getApplicationContext());
        } catch (MLException e) {
            e.printStackTrace();
        }

        Feature accMean = new FeatureNumeric("accMean");
        Feature accVariance = new FeatureNumeric("accVariance");
        Feature accMCR = new FeatureNumeric("accMCR");

        ArrayList<String> classValues = new ArrayList<String>();
        classValues.add(getResources().getString(R.string.txt_gesture_1));
        classValues.add(getResources().getString(R.string.txt_gesture_2));
        classValues.add(getResources().getString(R.string.txt_gesture_3));

        Feature movement = new FeatureNominal("movement", classValues);

        ArrayList<Feature> features = new ArrayList<Feature>();
        features.add(accMean);
        features.add(accVariance);
        features.add(accMCR);
        features.add(movement);

        Signature signature = new Signature(features, features.size()-1);

        try {
            mManager.addClassifier(Constants.TYPE_NAIVE_BAYES, signature,
                    new ClassifierConfig(),"movementClassifier");
        } catch(MLException e) {
            e.printStackTrace();
        }
    }


    public void recordAccData(float mean, float variance, float MCR) {

        Log.d(TAG, "recordAccData Intensity: " + mean + " var " + variance + " MCR " + MCR);

        Switch s = findViewById(R.id.sw_training);

        if (s.isChecked()) {

            // get the label of the selected radio button
            RadioGroup rg = findViewById(R.id.radioGroup);
            int selectedId = rg.getCheckedRadioButtonId();

            RadioButton rb = (RadioButton) findViewById(selectedId);
            String label = rb.getText().toString();
            Log.d(TAG, "Sending training data to model for: "+label+"movement.");

            // send data to TrainClassifierService
            Intent mIntent = new Intent(MainActivity.this,
                    TrainClassifierService.class);
            mIntent.putExtra("accMean", mean);
            mIntent.putExtra("accVar", variance);
            mIntent.putExtra("accMCR", MCR);
            mIntent.putExtra("label", label);
            mIntent.setAction(ACTION_CLASSIFIER_TRAINING);
            startService(mIntent);


        } else {

            // The inference (classification) and set of the result (also screen background colour)
            Classifier c = mManager.getClassifier("movementClassifier");
            ArrayList<Value> instanceValues = new ArrayList<Value>();

            Value meanValue = new Value((double) mean, Value.NUMERIC_VALUE);
            Value varianceValue = new Value((double)variance, Value.NUMERIC_VALUE);
            Value MCRValue = new Value((double)MCR, Value.NUMERIC_VALUE);

            instanceValues.add(meanValue);
            instanceValues.add(varianceValue);
            instanceValues.add(MCRValue);

            Instance instance = new Instance(instanceValues);
            Value inference = null;

            try {
                inference = c.classify(instance);
            } catch (MLException e) {
                e.printStackTrace();
            }

            Log.d(TAG, "GOT INFFERED: "+inference.getValue().toString());

            TextView tvResult = findViewById(R.id.tv_result);
            tvResult.setTextColor(Color.WHITE);
            View v = findViewById(R.id.container);

            if(inference.getValue().toString().equals(getString(R.string.txt_gesture_1))) {
                Log.d(TAG, "changing because of RAPID");
                tvResult.setText(R.string.txt_gesture_1);
                v.setBackgroundColor(Color.BLUE);
            }
            else if(inference.getValue().toString().equals(getString(R.string.txt_gesture_2))) {
                Log.d(TAG, "changing because of SLOW");
                tvResult.setText(R.string.txt_gesture_2);
                v.setBackgroundColor(Color.RED);
            }
            else if(inference.getValue().toString().equals(getString(R.string.txt_gesture_3))) {
                Log.d(TAG, "changing because of CIRCULAR");
                tvResult.setText(R.string.txt_gesture_3);
                v.setBackgroundColor(Color.GREEN);
            }
        }
    }


    public class AccBroadcastReceiver extends BroadcastReceiver {

        public static final String ACTION_SENSING_RESULT = "si.uni_lj.fri.lrk.lab7.SENSING_RESULT";
        public static final String MEAN = "mean";
        public static final String VARIANCE = "variance";
        public static final String MCR = "MCR";

        @Override
        public void onReceive(Context context, Intent intent) {

            Log.d(TAG, " AccBroadcastReceiver onReceive...");

            float mean = intent.getFloatExtra(MEAN, 0);
            float variance = intent.getFloatExtra(VARIANCE, 0);
            float mcr = intent.getFloatExtra(MCR, 0);

            Log.d(TAG, "recordAccData Intensity: " + mean + " var " + variance + " MCR " + mcr);
            recordAccData(mean, variance, mcr);
        }
    }

}
